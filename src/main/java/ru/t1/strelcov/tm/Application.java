package ru.t1.strelcov.tm;

import ru.t1.strelcov.tm.api.ICommandRepository;
import ru.t1.strelcov.tm.constant.ArgumentConst;
import ru.t1.strelcov.tm.constant.TerminalConst;
import ru.t1.strelcov.tm.model.Command;
import ru.t1.strelcov.tm.repository.CommandRepository;
import ru.t1.strelcov.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        displayWelcome();
        if (parseArgs(args))
            exit();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            parseCommand(scanner.nextLine());
            System.out.println();
        }
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                displayHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                displayVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                displayAbout();
                break;
            case ArgumentConst.ARG_INFO:
                displaySystemInfo();
                break;
            default:
                System.out.println("Error: Command doesn't exist. Enter command: help");
                break;
        }
    }

    private static void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_INFO:
                displaySystemInfo();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            case TerminalConst.CMD_COMMANDS:
                displayCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                displayArguments();
                break;
            default:
                System.out.println("Error: Command doesn't exist. Enter command: help");
                break;
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        return true;
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

    private static void displayCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    private static void displayArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command : commands) {
            final String name = command.getArg();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.3.0");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Sla La");
        System.out.println("slala@slala.ru");
    }

    private static void displaySystemInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
