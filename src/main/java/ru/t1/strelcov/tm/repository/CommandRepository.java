package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.ICommandRepository;
import ru.t1.strelcov.tm.constant.ArgumentConst;
import ru.t1.strelcov.tm.constant.TerminalConst;
import ru.t1.strelcov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Display list of terminal commands.");

    private static final Command ABOUT = new Command(TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Display developer info.");

    private static final Command VERSION = new Command(TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Display program version.");

    private static final Command INFO = new Command(TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "Display system info.");

    private static final Command EXIT = new Command(TerminalConst.CMD_EXIT, null, "Exit program.");

    private static final Command COMMANDS = new Command(TerminalConst.CMD_COMMANDS, null, "Display commands.");

    private static final Command ARGUMENTS = new Command(TerminalConst.CMD_ARGUMENTS, null, "Display arguments.");

    private static final Command[] TERMINAL_COMMANDS = {
            HELP, ABOUT, VERSION, INFO, COMMANDS, ARGUMENTS, EXIT
    };

    public Command[] getCommands() {
        return TERMINAL_COMMANDS;
    }

}
